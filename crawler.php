<?php
include("lib/simple_html_dom.php"); 
require_once("Db.class.php");


function renderPagination($page = 1, $total_pages, $limit = 15, $adjacents = 1, $targetpage = "/")
{

	/* Setup page vars for display. */
	// How many adjacent pages should be shown on each side?
	$adjacents = 3;
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1

	$pagination='';
	if($lastpage > 1)
	{	
		$pagination .= "<div class=\"pagination\">";
		//previous button
		if ($page > 1) 
			$pagination.= "<a href=\"$targetpage?page=$prev\">&lt;&lt; previous</a>";
		else
			$pagination.= "<span class=\"disabled\">&lt;&lt; previous</span>";	
		
		//pages	
		if ($lastpage < 7 + ($adjacents * 2))	//not enough pages to bother breaking it up
		{	
			for ($counter = 1; $counter <= $lastpage; $counter++)
			{
				if ($counter == $page)
					$pagination.= "<span class=\"current\">$counter</span>";
				else
					$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
			}
		}
		elseif($lastpage > 5 + ($adjacents * 2))	//enough pages to hide some
		{
			//close to beginning; only hide later pages
			if($page < 1 + ($adjacents * 2))		
			{
				for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
			}
			//in middle; hide some front and some back
			elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
			{
				$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
				$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
				$pagination.= "...";
				$pagination.= "<a href=\"$targetpage?page=$lpm1\">$lpm1</a>";
				$pagination.= "<a href=\"$targetpage?page=$lastpage\">$lastpage</a>";		
			}
			//close to end; only hide early pages
			else
			{
				$pagination.= "<a href=\"$targetpage?page=1\">1</a>";
				$pagination.= "<a href=\"$targetpage?page=2\">2</a>";
				$pagination.= "...";
				for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= "<span class=\"current\">$counter</span>";
					else
						$pagination.= "<a href=\"$targetpage?page=$counter\">$counter</a>";					
				}
			}
		}
		
		//next button
		if ($page < $counter - 1) 
			$pagination.= "<a href=\"$targetpage?page=$next\">next >></a>";
		else
			$pagination.= "<span class=\"disabled\">next &gt;&gt;</span>";
		$pagination .= "</div>\n";		
	}
	return $pagination;
}

abstract class Crawler
{
   public function getPriceByUrl($url)
   {
   }
   function get_url_contents($url){
        $crl = curl_init();
        $timeout = 5;
        curl_setopt ($crl, CURLOPT_URL,$url);
        curl_setopt ($crl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt ($crl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:26.0) Gecko/20100101 Firefox/26.0');
        $ret = curl_exec($crl);
        curl_close($crl);
        return $ret;
	}
}

class MemuaCrawler extends Crawler
{
	public $priceRegex;
	public function __construct()
	{
 		//$this->priceRegex = '/<ul class="product-infomation">.*<span class="price">(.*)\sđ<\/span>/smU';
	}
	public function getPriceByUrl($url)
	{
		$html = file_get_contents($url); 
		$dom = new DOMDocument(); 
		@$dom->loadHTML($html); 
		$xpath = new DOMXPath($dom);  
		$elements = $xpath->query("//span[@class='price']");
		$price = strstr($elements->item(0)->nodeValue,' ', true);
		$price = str_replace(array(',','.'),'',$price);
		return $price;
	}
}

class LazadaCrawler extends Crawler
{
	public $priceRegex;
	public function __construct()
	{

	}
	public function getPriceByUrl($url)
	{
		$html = file_get_contents($url); 
		$dom = new DOMDocument(); 
		@$dom->loadHTML($html); 
		$xpath = new DOMXPath($dom);  
		$elements = $xpath->query("//span[contains(@id, 'special_price_box')]");
		$price = strstr($elements->item(0)->nodeValue,' ', true);
		$price = str_replace(array(',','.'),'',$price);
		return $price;
	}
}
class Vng123Crawler extends Crawler
{
	public $priceRegex;
	public function __construct()
	{

	}
	public function getPriceByUrl($url)
	{
		$html = file_get_contents($url); 
		$dom = new DOMDocument(); 
		@$dom->loadHTML($html); 
		$xpath = new DOMXPath($dom);  
		$elements = $xpath->query("//input[contains(@id, 'chkprice')]");
		$price = $elements->item(0)->getAttribute('value');
		$price = str_replace(array(',','.'),'',$price);
		return $price;
	}
}
//chkprice
//$lazadaCrawler = new LazadaCrawler();
//echo $lazadaCrawler->getPriceByUrl('http://www.lazada.vn/donghoteen-848parisden-dong-ho-den-73748.html');

//$memuaCrawler = new MemuaCrawler();
//echo $memuaCrawler->getPriceByUrl('http://memua.vn/sua-thuc-pham/sua/sua-cho-be/sua-dinh-duong-en/sua-bot-humana-expert-1-350g-cho-tre-0-6-thang.html');

//$vng123Crawler = new Vng123Crawler();
//echo $vng123Crawler->getPriceByUrl('http://123.vn/bvs-kotex-pro-sieu-mong-goi-lon-2016-tang-kem-tui-luoi-thoi-trang_89152.html?page=product&box=cross_sale&pos=2&ref_id=74827');
$db = new Db();
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	//is_numeric
	$lazadaCrawler = new LazadaCrawler();
	$memuaCrawler = new MemuaCrawler();
	$vng123Crawler = new Vng123Crawler();
	$sku = $_POST['sku'];
	if(!empty($_POST['lazada']))
	{
		$lazada = $lazadaCrawler->getPriceByUrl($_POST['lazada']);
		if(!is_numeric($lazada))
			$lazada = '';
	}
	else
		$lazada = '';

	if(!empty($_POST['vng123']))
	{
		$vng123 = $vng123Crawler->getPriceByUrl($_POST['vng123']);
		if(!is_numeric($vng123))
			$vng123 = '';
	}
	else
		$vng123 = '';

	if(!empty($_POST['memua']))
	{
		$memua = $memuaCrawler->getPriceByUrl($_POST['memua']);
		if(!is_numeric($memua))
			$memua = '';
	}
	else
		$memua = '';


	$fields     =  array('sku','lazada','vng123','memua');
	$fieldsvals =  array(implode(",",$fields),":" . implode(",:",$fields));
	$bindingValues = array('sku'=>$sku,'lazada'=> $lazada,'vng123'=> $vng123,'memua'=> $memua);
	$table = 'price_crawl';
	$sql 		= "INSERT INTO ".$table." (".$fieldsvals[0].") VALUES (".$fieldsvals[1].")";
	$db->query($sql, $bindingValues);
}



$result = $db->query('SELECT * FROM price_crawl order by id DESC');




?>
<?php
	$table = 'price_crawl';

	/* 
	   First get total number of rows in data table. 
	   If you have a WHERE clause in your query, make sure you mirror it here.
	*/
	$totalPageSql = "SELECT COUNT(*) as num FROM $table";
	$total_pages = $db->query($totalPageSql);

	$total_pages = $total_pages[0]['num'];


	$limit = 5; 								//how many items to show per page
	$page = (isset($_GET['page']))? $_GET['page'] : 0;
	if($page) 
		$start = ($page - 1) * $limit; 			//first item to display on this page
	else
		$start = 0;								//if no page var is given, set start to 0



	/* Get data. */
	$resutSql = "SELECT * FROM  $table LIMIT $start, $limit";
	$result = $db->query($resutSql);


	$targetpage = "crawler.php";
	/* Setup page vars for display. */
	// How many adjacent pages should be shown on each side?
	$adjacents = 3;
	if ($page == 0) $page = 1;					//if no page var is given, default to 1.
	$prev = $page - 1;							//previous page is page - 1
	$next = $page + 1;							//next page is page + 1
	$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
	$lpm1 = $lastpage - 1;						//last page minus 1







?>
<html>
<head>

<link rel="stylesheet" type="text/css" href="/css/paging.css" />
	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['SKU', 'lazada.vn', '123.vn', 'memua.vn'],
          
<?php foreach($result as $product):
	echo '[\''.$product['sku'].'\', '.$product['lazada'].', '.$product['vng123'].', '.$product['memua'].'],';
 ?>
<?php endforeach; ?>
        ]);

        var options = {
          title: 'Price Comparison',
          hAxis: {title: 'Product SKU', titleTextStyle: {color: 'red'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
</head>

<body>


<table class="bordered">
    <thead>
    <tr>
        <th>#</th>        
        <th>SKU</th>
        <th>lazada.vn <small>(product's link)</small></th>
        <th>123.vn <small>(product's link)</small></th>
        <th>memua.vn <small>(product's link)</small></th>
    </tr>
    </thead>
    <tbody>
    	<tr>
    		<form action="" method="post" id="tfnewsearch">
    			<td>
    				
    			</td>
    			<td>
    				<input  type="text" name="sku" maxlength="255" size="30" value="">  
    			</td>
    			<td>
    				<input  type="text" name="lazada" maxlength="255" size="30" value="">  
    			</td>
    			<td>
    				<input  type="text" name="vng123" maxlength="255" size="30" value="">  
    			</td>
    			<td>
    				<input  type="text" name="memua" maxlength="255" size="30" value="">  
    			</td>
    			<td>
    				 <input type="submit" class="tfbutton" value="add to crawler">
    			</td>
    		</form>
    	</tr>
<?php foreach($result as $product): ?>
	    <tr>
	        <td><?php echo $product['id'] ?></td>        
	        <td><?php echo $product['sku'] ?></td>
			<td><?php echo $product['lazada'] ?></td>
			<td><?php echo $product['vng123'] ?></td>
	        <td><?php echo $product['memua'] ?></td>
	    </tr>        
<?php endforeach; ?>
</tbody></table>


<?php 

echo renderPagination($page, $total_pages, $limit , $adjacents, $targetpage);
//echo $pagination; ?>

<div id="chart_div" style="width: 900px; height: 500px;"></div>



</body>
</html>