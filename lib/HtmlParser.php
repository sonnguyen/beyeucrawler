<?
// byron whitlock 2/13/2011
//
// parse HTML with this class
//
// 
class HtmlParser 
{
	public $html = "";
	public $dom = NULL;
	public $xpath = NULL;


	function __construct($html)
	{
		
		$this->dom = new DOMDocument();

		// the xdom parser is finiky
		$html = preg_replace("/(\t|\n|\r)+/"," ",$html);
		
		// turn non breaking spaces in to real spaces.
		$html = preg_replace("/&nbsp;/i"," ", $html);

		// it hates all sorts of html entities. lets just remove them.
		# $html = preg_replace("/&[a-z]+;/i","", $html);
		
		$this->html = $html;
		@$this->dom->loadHTML($html);
		$this->xpath = new DOMXPath($this->dom);	
		
	}
	
	// do an xpath query 
	// pass thru for $this->xpath->query;
	public function query($xpath_query)
	{
		return $this->xpath->query($xpath_query);
	}

	//
	// returns an simple array based on the <select> list found by the xpath query given
	//
	public function getSelectArray($xpath_query)
	{
		$x = $this->xpath($xpath_query);				
		$data = array();

		foreach($x->query($query) as $node)
		{
			$name = $node->textContent;
			$value = $node->getAttribute('value');

			if (empty($value))
			{
				$value = $name;
			}
			$selectList[$value] = $name;			
		}

		return $selectList;
	}


	// this should be in a parent class called baseScrapeAspx that has code for aspx sites.
	// loads the viewstate based on the current document.
	public function loadViewState()
	{

		$x = $this->xpath;		
		
		$data = array();
		// grab categories from subnavigation
		foreach($x->query("//*[@name='__VIEWSTATE']") as $node)
		{
			$data['__VIEWSTATE'] = $node->getAttribute('value');
			break;
		}

		foreach($x->query("//*[@name='__EVENTVALIDATION']") as $node)
		{
			$data['__EVENTVALIDATION'] = $node->getAttribute('value');
			break;
		}

		$data['__SCROLLPOSITIONX']=0;
		$data['__SCROLLPOSITIONY']=0;

		$data['__EVENTTARGET']='';
		$data['__EVENTARGUMENT']='';
		
		return $data;
		/*		
		// set the following variables
		$data['ctl00$MainContentArea$LoginBox'] = $user;
		$data['ctl00$MainContentArea$PasswordBox'] = $pass;
		$data['ctl00$MainContentArea$LoginButton'] = "Login";
		
		$this->Post("https://www.abda.org/login.aspx?".$this->buildQuery($data));
		*/
	}

}