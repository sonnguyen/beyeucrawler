<?
include_once("search_engine.php");

class search_engine_bing implements search_engine
{
	// searches bing and returns an array of results.
	function search($search_term)
	{
		$url = $this->url($search_term);
		$xml = file_get_contents($url);
		return $this->parse($xml);
	}

	function url($search_term,$page=1)
	{
		$search_term = urlencode($search_term );
		$start = 10* $page;
		return "http://www.bing.com/search?first=$start&q=$search_term";
	}

	function parse($html)
	{		
		$doc = new DOMDocument();
		@$doc->loadHtml($html);

		$x = new DOMXpath($doc);
		
		$output = array();


		foreach($x->query("//li[@class='sa_wr']//div[@class='sb_tlst']//a") as $node)
		{
			$output[]['url'] = $node->getAttribute("href");
		}

		return $output;
	}
}