<?

// this class implements a thread safe queue.
class load_queue
{
   private $queue = array();

   // slightly slow way to push all urls into the database.
   /// if they are already there, it overwrites them.
   // returns number of urls loaded into db.
   public function pushAll($urls,$type)
   {
      $count = 0;
      foreach ($urls as $url)
      {
         $this->queue[] = $url;
         $count++;
      }
      return $count;

   }

   // push a single url to the queue
   
   public function push($url,$type)
   {
      $this->pushAll(array($url), $type);
   }

   # grabs a url off the load queue of type $type
   public function pop($type)
   {      
      return array_pop($this->queue);
   }

   static function expireUrl($url)
   {
       //no op
   }
   // expires urls that are older than days days.
   function expireQueue($days,$type,$url)
   {
      //no op
   }

   function numToProcess($type)
   {
      return sizeof($this->queue);

   }
}