<?
/*
Simple class for doing mysql db operations

This is a static class because the screeen scraper needs
to do db operations in a static callback.

3-15-2011 - updated to use redbean.
*/
require_once("rb.php");
require_once("log.inc");

class db 
{
   private static $inited = false;
	private static $database = '';
	private static $RB_indexed = false;

   static function init($host='localhost',$user,$pass,$dbname)
   {
		//we can use both redbean and mysql. 
		R::setup("mysql:host=localhost;dbname=$dbname",$user,$pass);
		db::$database = $dbname;

		$conn = mysql_connect($host, $user, $pass);

      if (!$conn) {
          log::fatal("Unable to connect to DB: " . mysql_error());
      }
        
      if (!mysql_select_db($dbname)) {
          log::fatal( "Unable to select mydbname: " . mysql_error() );
      }

      db::$inited = true;
   }



   public static function isInited ()
   {
      return db::$inited;
   }

   // QUOTES A STRING FOR THE DB
   static function quote($txt)
   {
		//
		if (!empty($txt))
		{
	      return mysql_real_escape_string(trim($txt));
		}
   }

   // this function does an update select insert or delete. on delete or update, it returns the number of rows modified, on select it returns an associative array. on insert it returns the mysql_last_insert_id();
   // CURRENTLYD Does not work when the query returns multiple rows
   static function query($sql)
   {
      $sql = trim($sql);
      log::debug_verbose("Running Query $sql");
      $result = mysql_query($sql);
      if ($result)
      {
         if (preg_match("/^SELECT/i",$sql))
         {
            $row = mysql_fetch_assoc($result);
            mysql_free_result($result);

            return $row;
         }
         else if (preg_match("/^REPLACE|INSERT/i",$sql))
         {
            return mysql_insert_id();
         }
         else if (preg_match("/^UPDATE|DELETE/i",$sql))
         {
            return mysql_affected_rows();
         }
			else
			{
				log::error("First command must be SELECT|REPLACE|INSERT|UPDATE|DELETE." . mysql_error());
				log::error($sql);
				return "";
			}
      }
      else
      {
         log::error("Cannot execute $sql" . mysql_error());
         return "";
      }

   }
	
	static function oneCol($sql)
	{
      $result = mysql_query($sql);
      if ($result)
      {
			$return = array();
			while ($row = mysql_fetch_row($result))
			{
				$return[] = $row[0];
			}
         mysql_free_result($result);
         return $return;
      }
      else
      {
         log::error("Cannot execute $sql" . mysql_error());
         return array();
      }
	}
   static function oneCell($sql)
   {
      $result = mysql_query($sql);
      if ($result)
      {
         $row = mysql_fetch_row($result);
         mysql_free_result($result);
         return $row[0];
      }
      else
      {
         log::error("Cannot execute $sql" . mysql_error());
         return "";
      }
   }
   static function insertInto($table, $fields , $delayed=false,$ignore=false)
   {
      
      $extraSql = "";
      if ($delayed)
      {
         $extraSql .= ' DELAYED ';
      }

      if ($ignore)
      {
         $extraSql .= ' IGNORE ';
      }

      $table = db::quote($table);
      foreach ($fields as $field => $value)
      {
         $cleanFieldNames[] = db::quote($field);
         $cleanFieldValues[] = "'".db::quote($value)."'";
      }

      $sql = "INSERT $extraSql INTO $table (";
      $sql .= join(",",$cleanFieldNames);
      $sql .= ") VALUES (";
      $sql .= join(",",$cleanFieldValues);
      $sql .= ")";

      return db::query($sql);
   }

	//store data in the table. if keyfields is an array are not null, data will be overwritten.
	// uses redbean so tables/fields are created on the fly
	static function store($table, $data,$keyfields=array(),$overwrite = true)
	{
		$sqlNames = "";
		$sqlValues = array();
		
		if (sizeof($keyfields))
		{

			foreach ($data as $field=>$value)
			{
				if (in_array($field, $keyfields) )
				{
					$sqlNames[] = " $field = ? ";
					$sqlValues[] = $value;
				}
			}
			$sqlNames = join(" AND ",$sqlNames);
		
			if (! db::$RB_indexed)
			{
				log::info("INDEXING DATA");
				$sql = "ALTER TABLE $table ADD UNIQUE INDEX RB_idx(".join(",", $keyfields).")";
				mysql_query($sql);	
				db::$RB_indexed = true;

			}

			foreach(R::find($table, $sqlNames, $sqlValues) as $bean)
			{
				// owverwrite any existing beans.
				if ($overwrite)
				{
					R::trash($bean);
					log::info("Overwritting");
				}
				else
				{
					return;
				}
			}
		
		}
		$bean = R::dispense($table);
		

		foreach($data as $field => $value)
		{
			$bean->$field = $value;
		}

		

		return R::store($bean);
		

	}

   static function replaceInto($table, $fields , $delayed=false,$ignore=false)
   {

      $extraSql = "";
      if ($delayed)
      {
         $extraSql .= ' DELAYED ';
      }

      if ($ignore)
      {
         $extraSql .= ' IGNORE ';
      }


      $table = db::quote($table);
      foreach ($fields as $field => $value)
      {
         $cleanFieldNames[] = db::quote($field);
         $cleanFieldValues[] = "'".db::quote($value)."'";
      }

      $sql = "REPLACE $extraSql INTO $table (";
      $sql .= join(",",$cleanFieldNames);
      $sql .= ") VALUES (";
      $sql .= join(",",$cleanFieldValues);
      $sql .= ")";

      return db::query($sql);
   }

  static function update($table, $fields, $whereField,$whereValue)
   {
      if (sizeof($fields) < 1)
      {
         return 0;
      }
      $table = db::quote($table);
      $sql = "UPDATE $table SET ";
      foreach ($fields as $field => $value)
      {
         $sqla[] = db::quote($field)."='".db::quote($value)."'";
      }
      $sql .= join(",",$sqla);
      $sql .= " WHERE ".db::quote($whereField) ."= '".db::quote($whereValue)."'";
      
      log::debug_verbose($sql);

      return db::query($sql);
   }

   static function BEGIN()
   {
      mysql_query("BEGIN");
   }  
   
   static function COMMIT()
   {
      mysql_query("ROLLBACK");
   }

   static function ROLLBACK()
   {
      mysql_query("ROLLBACK");
   }

}


