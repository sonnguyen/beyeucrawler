<?

class parse
{

// loop to pull data off the raw_data queue
//		raw_data is the result of a parse
	public function parseData($callback)
   {
      $i = 0;
      // so we use a forever loop to check the raw_data  (the results of the load queue) to see if there is any more data left to parse
      for (;;)
      {
			$type = get_class(baseScrape::getInstance());

			try
			{				
				// now we just iterate raw_data, pull out what we want and stick in the correct table.
				// use for update to keep this thread safe.
				$row = db::query("SELECT url,html FROM raw_data WHERE type = '$type' and parsed = 0 LIMIT 1 FOR UPDATE ");
				
				

				$url = $row['url'];
				$html = str_replace('&nbsp;'," <br> ", $row['html']);

				log::debug("Parsing: $url");

				// we think we have parsed everything.
				// we have to make sure since the user function could have pushed
				// data on the load_queue since we last checked.
				if ($url == "" || $html == "")
				{
					baseScrape::getInstance()->queuedGet();
					$sql = "SELECT count(*) FROM raw_data WHERE type = '$type' and parsed = 0 LIMIT 1";
					$count = db::oneCell($sql);

					// we done parsed everything.
					if ($count == 0)
					{
						break;
					}
				}
			
				call_user_func($callback, $url, $html);
				db::query("UPDATE raw_data SET parsed = 1 WHERE type='$type' AND url = '".db::quote($url)."'");
			}
			catch (exception $e) 
			{
				// we have to do an update in case of an error to prevent the row from being locked indefinitly.
				db::query("UPDATE raw_data SET parsed = 0 WHERE type='$type' AND url = '".db::quote($url)."'");
				throw $e;
			}

      }
   }  
}