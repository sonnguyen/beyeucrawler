<?

interface search_engine
{
	public function search($search_term);

	# this function returns the url with the search term for the page
	# the search term must be the last item of the url.

	public function url($search_term,$page=1);
	public function parse($html);
}