<?
/*
Simple class for doing mysql db operations
*/
class db 
{

   private isInited = false;
   private dbConnection = false;
   private db = "default.db";

   public static init($db=null)
   {
      if (!db::$isInited)
      {
         if ($db == null)
         {
            $db = db::$db;
         }
              
         db::setConn( sqlite_open  (  $db ) );
         db::$isInited = true;   
      }
   }

   private static function setConn($conn)
   {
      db::$dbConnection = $conn;
   }

   private static function conn()
   {
      return db::$dbConnection;
   }


   // QUOTES A STRING FOR THE DB
   public static function quote($txt)
   {
      return mysql_real_escape_string(trim($txt));
   }

   // this function does an update select insert or delete. on delete or update, it returns the number of rows modified, on select it returns an associative array. on insert it returns the mysql_last_insert_id();
   // CURRENTLYD Does not work when the query returns multiple rows
   //
   /* 
      options can be 
         selectAllRows - normally only the first row is returned, this will return all rows. 
            NOTE: This can be memory intensive. All db rows are copied to memory.
            TODO: implment an iterator interface so you can iterate large query objects without taking tons of memory.
   */
   public static function query($sql,$selectAllRows=false)
   {

      init();
      $sql = trim($sql);
      $result = sqllite_query($sql);
      if ($result)
      {
         if (preg_match("/^SELECT/i",$sql))
         {
            if ($selectAllRows)
            {
               return sqllite_fetch_all($result);
            }
            else
            {
               return  sqllite_fetch_array ($result);
            }
         }
         else if (preg_match("/^REPLACE|INSERT/i",$sql))
         {
            return sqlite_last_insert_rowid();
         }
         else if (preg_match("/^UPDATE|DELETE/i",$sql))
         {
            return sqlite_num_row();
         }
      }
      else
      {
         return("Cannot execute $sql" . sqlite_last_error());
      }

   }

   public static function replaceInto($table, $fields)
   {
      return db::insertInto($sql$,$fields, "REPLACE");
   }

   public static function insertInto($table, $fields, $options = "INSERT")
   {

      $table = db::quote($table);
      foreach ($fields as $field => $value)
      {
         $cleanFieldNames[] = db::quote($field);
         $cleanFieldValues[] = "'".db::quote($value)."'";
      }

      $sql = "$options INTO $table (";
      $sql .= join(",",$cleanFieldNames);
      $sql .= ") VALUES (";
      $sql .= join(",",$cleanFieldValues);
      $sql .= ")";

      return db::query($sql);
   }

   public static function BEGIN()
   {
      sqllite_query("BEGIN");
   }  
   
   public static function COMMIT()
   {
      sqllite_query("ROLLBACK");
   }

   public static function ROLLBACK()
   {
      sqllite_query("ROLLBACK");
   }

}


