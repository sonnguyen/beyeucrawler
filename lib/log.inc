<?

define("ERROR_NONE",4);
define("ERROR_INFO",3);
define("ERROR_DEBUG",2);
define("ERROR_DEBUG_VERBOSE",1);
define("ERROR_ALL",0);

class log {

   public static $errorLevel = ERROR_INFO;

	public static $html = false;


	
	public static function error_level($e)
	{
		log::$errorLevel =  $e;
	}

	public static function fatal($error)
	{
		log::write( "\n!!!!FATAL ERROR!!!! $text");
		exit;
	}

   public static function error($text)
   {
      if (log::$errorLevel < ERROR_NONE)
      {
         log::write("\n!!!!ERROR!!!! $text");
      }
   }
   public static function debug($text,$rawEcho=false)
   {
      if (log::$errorLevel <= ERROR_DEBUG)
      {
         if ($rawEcho)
         {
            log::write($text);
         }
         else
         {
            log::write("\nDEBUG: $text");
         }
      }
   }

   public static function debug_verbose($text)
   {
      if (log::$errorLevel <= ERROR_DEBUG_VERBOSE)
      {
         log::write("\nDEBUG: $text");
      }
   }
   
   public static function info($text)
   {
      if (log::$errorLevel <= ERROR_INFO)
      {
			log::write("\nINFO: ");
			if (is_array($text))
			{
				print_r($text);
			}
         else
				log::write($text);
      }
   }
   
	public static function write($text)
	{
		if (log::html())
		{
			$out = "\n<h1 color='RED'>".trim($text)."</h1>";
		}
		else
		{
			$out = $text;
		}

		echo $out;

	}

	public static function html($html=null)
	{
		if ($html != null)
		{
			log::$html = $html;
		}
		return $html;
	}
}