<?
include_once("search_engine.php");

class search_engine_google implements search_engine
{
	// searches bing and returns an array of results.
	function search($search_term)
	{
		$xml = file_get_contents($this->url($search_term));
		return $this->parse($xml);
	}

	function url($search_term,$page=1)
	{
		$start = 10* $page;
		return "http://www.google.com/search?start=$start&q=".urlencode($search_term);
	}

	function parse($html)
	{		
		$doc = new DOMDocument();
		@$doc->loadHtml($html);

		$x = new DOMXpath($doc);
		
		$output = array();

		// just grab the urls for now
		//foreach ($x->query("//div[@class='vspi']") as $node)
		foreach ($x->query("//a") as $node)			
		{

			
			$href = $node->getAttribute("href");

			
			// is it an offsite link?
			//$path = parse_url($href, PHP_URL_PATH);

			if (preg_match("/^http:/",$href,$matches))
			{
				$host = parse_url($href, PHP_URL_HOST);

				if (preg_match("/^(.+\.)?google\.com\$/",$host))
				{
					continue;
				}
				if (preg_match("/^(.+\.)?googleusercontent\.com\$/",$host))
				{
					continue;
				}

				$output[]['url'] = $href;
			}			
		}

		return $output;
	}
}
/*
$google = new search_engine_google();
$dogs = $google->search("site:youtube.com dogs",4);

print_r($dogs);*/