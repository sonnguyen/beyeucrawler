<?
# Property of Whitlock Consulting
# 3-15-2011 
include_once('parse.inc');

class baseScrape
{

   private $queue = NULL;

   public $timeout = 30; // default to 30 second timeouts
   public $threads  = 4;  // four threads by default
   protected $cookie_file = NULL;
   public $useCookies = true;
   public $debug=false;
   public $allowRedirects = 1;
   public $csvPath = 'csv';
   public $proxy = false;
   public $useCurl = true;
   public $responseHeaders = null;
   public $useDB = true;

	// when doing a post, sendQueryStringOnPost=true will send the full URL including querydata in the request, 
	// if true, only the url is posted. setting this to true will cause issues with the queueud posting
	// because when a url is processed by the callback, it will not be unique; the queued posting code will not save the uniqure request: it will be overwritten and there will be only 1 item in raw_data
	// defaults to false for legacy projects
	public $sendQueryStringOnPost=false; 

   // list of user agent strings here: http://www.useragentstring.com/pages/useragentstring.php
   public $userAgent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.8) Gecko/20100722 Firefox/3.6.8 ( .NET CLR 3.5.30729)';

   protected $referer = null;
   protected $cookieData = null;
   
   protected static $_this = null;
   public static function getinstance() { return baseScrape::$_this; }


   public function __construct()
   {
      baseScrape::$_this = $this;

      $this->queue = new load_queue();
      

      if ($this->cookie_file  == null)
      {
         $this->cookie_file = dirname(__FILE__)."/cookies.txt";
      }

      $this->headers[] = "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
      $this->headers[] = "Accept-Language: en-us,en;q=0.5";

      $this->headers[] = "Accept-Encoding: gzip,deflate";
      $this->headers[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";

   }

   public function loadUrl($url,$forceLoad=false)
   {
      $type = get_class($this);
      return $this->queue->push($url,$type,$forceLoad);
   }
   // expired after 
   public function expireQueue($days = 2)
   {
      $type = get_class($this);
      $this->queue->expireQueue($days,$type);
   }

   public function loadUrlsByArray($urls)
   {
      $type = get_class($this);
      log::info ("Loading urls for $type");
      return  $this->queue->pushAll($urls,$type);
   }

   public function loadUrlsByZip($urlTemplate, $minPop = 24999,$refererTemplate = false)
   {
      $type = get_class($this);
      log::info ("Loading urls by zip codes for $type");
      
      $count=0;
      $result = mysql_query("SELECT distinct zip FROM geo.locations where pop > $minPop");
      
      while ($r = mysql_fetch_row($result))
      {
         $url = str_replace("%ZIP%",$r[0],$urlTemplate);
         $urls[] = $url;

			if ($refererTemplate)
			{
				$refererUrl = str_replace("%ZIP%",$r[0],$refererTemplate);
				$this->setReferer($url, $refererUrl);
			}
        
      }
      return  $this->queue->pushAll($urls,$type);
   }

   public function loadUrlsByStateZip($urlTemplate, $state = "CA",$refererTemplate = false)
   {
      $type = get_class($this);
      log::info ("Loading urls by zip codes for $type, state $state");
      
      $count=0;
      $result = mysql_query("SELECT distinct zip FROM geo.locations where state = '$state'");
      
      while ($r = mysql_fetch_row($result))
      {
         $url = str_replace("%ZIP%",$r[0],$urlTemplate);
         $urls[] = $url;

			if ($refererTemplate)
			{
				$refererUrl = str_replace("%ZIP%",$r[0],$refererTemplate);
				$this->setReferer($url, $refererUrl);
			}
        
      }

      return  $this->queue->pushAll($urls,$type);
   }

   public function loadUrlsByAreaCode($urlTemplate)
   {
		log::error("loadUrlsByAreaCode is Broken.");
		

      $type = get_class($this);
      log::info ("Loading urls by AreaCode codes for $type");
      
      $count=0;
      $result = mysql_query("SELECT distinct areaCode FROM geo.locations");
      
      while ($r = mysql_fetch_row($result))
      {
         $url = str_replace("%AREACODE%",$r[0],$urlTemplate);
         $urls[] = $url;
        
      }
      return  $this->queue->pushAll($urls,$type);
   }

   public function loadUrlsByState($urlTemplate,$country='us')
   {
      $type = get_class($this);
      $country = db::quote($country);
      log::info ("Loading 50 urls by state code : $urlTemplate");
      
      $count=0;
      $result = mysql_query("SELECT distinct State FROM geo.locations WHERE country ='$country'");
      
      while ($r = mysql_fetch_row($result))
      {
         $url = str_replace("%STATE%",$r[0],$urlTemplate);
         $urls[] = $url;
        
      }
      return  $this->queue->pushAll($urls,$type);
   }

   public function loadUrlsByCity($urlTemplate, $state)
   {
      $type = get_class($this);
      $state = db::quote($state);
      log::info ("Loading urls by City codes for $type");
      
      $count=0;
      $result = mysql_query("SELECT distinct City FROM geo.locations WHERE state ='$state'");
      
      while ($r = mysql_fetch_row($result))
      {
         $url = str_replace("%CITY%",urlencode($r[0]),$urlTemplate);
         $urls[] = $url;
        
      }
      return  $this->queue->pushAll($urls,$type);
   }

   public function numToProcess()
   {
      $type = get_class($this);
      return $this->queue->numToProcess($type);
   }

  //  creates a csv with the name of the calling class
   public function generateCSV($stripNewlines=true,$fileName=false,$tableName=false)
   {
      $count = 0;

		$file = $table = get_class($this);

		if ($tableName)
		{
			$table = escapeshellcmd($fileName);
		} 	

		if ($fileName)
		{
			$file = escapeshellcmd($fileName);
		} 

      if (!is_dir($this->csvPath))
      {
         mkdir($this->csvPath);
      }

      $rc = fopen ("$this->csvPath/$file.csv",'w');

      if (!$rc)
      {
         log::error("CANNOT OPEN CSV $this->csvPath/$file.csv");
         return;
      }
         
      $result = mysql_query("SELECT * from $table");
      if ($result)
      while ($r = mysql_fetch_assoc($result))
      {
         // print the header.
         if ($count++ == 0)
         {
            fputcsv($rc, array_keys($r));
         }
         
         if ($stripNewlines)
         {
            $data = array();
            foreach(array_values($r) as $v)
            {
               $data[] = str_replace("\r"," ", str_replace("\n"," ", $v));
            }
            fputcsv($rc,$data);
         }
         else
         {
            fputcsv($rc,array_values($r));
         }
      }
      fclose($rc);

      log::info("Wrote $count lines to $file.csv");
   }

   // basic load call back to populate the db.
   public static function loadCallBack($url,$html,$type)
   {
      if (strlen($html) > 0)
      {
         log::info("Downloaded " . strlen($html) ." Bytes from $url");
         $url = mysql_real_escape_string($url);
         $html = mysql_real_escape_string($html);

         mysql_query("REPLACE INTO raw_data (url,html,type,parsed) VALUES('$url','$html','$type',0)");
      }
      else
      {
         log::info("Skipping $url");
      }
      flush();
   }

	public function parseFunction() 
	{
		return get_class($this). "::parse";
	}

	function parseData()
	{
		$parse = new parse();
		$parse->parseData($this->parseFunction());
	}

   public function queuedPost($callback=NULL)
   {	

		if ($this->threads == 1)
		{
			return $this->singleQueuedFetch( $callback, 'POST');
		}
		else
		{
	      return $this->queuedFetch( $callback, 'POST');
		}
   }

   public function queuedGet($callback=NULL)
   {
		if ($this->threads == 1)
		{
			return $this->singleQueuedFetch( $callback, 'GET');
		}
		else
		{
	      return $this->queuedFetch( $callback, 'GET');
		}
   }

	public function Post($url)
	{
		return  $this->singleFetch($url,'POST');
	}

	public function Get($url)
	{
		return  $this->singleFetch($url,'GET');
	}

	protected function curlInit($url,$method)
	{
		# remove any trailing # signs, but don't take ? query params out
		$actualURL = preg_replace("/\#.+\???/","",$url);

		$ch = curl_init();
		
		if ($this->proxy != false)
		{
			#curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, true);
			curl_setopt($ch, CURLOPT_PROXY, $this->proxy); 
		}

		if ($this->useCookies)
		{
			// this will reset cookies on each invocation
			// curl_setopt($ch, CURLOPT_COOKIESESSION,1);
			curl_setopt($ch, CURLOPT_COOKIEFILE, $this->cookie_file);
			curl_setopt($ch, CURLOPT_COOKIEJAR, $this->cookie_file); 

			curl_setopt($ch, CURLOPT_AUTOREFERER, true);
		}

		if ($this->cookieData)
		{
			curl_setopt($ch, CURLOPT_COOKIE, $this->cookieData);
		}
		
		$referer = $this->getReferer($url);
		if (!empty($referer) )
		{
			log::debug("Getting referrer for $url. GOT: ".$referer);
			curl_setopt($ch,CURLOPT_REFERER,$referer); 
		}

		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $this->allowRedirects);// allow redirects 
		
		curl_setopt($ch, CURLOPT_HEADERFUNCTION, array(&$this,'readHeader'));
		curl_setopt($ch, CURLOPT_USERAGENT, $this->userAgent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
		

		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers); 
		curl_setopt($ch, CURLOPT_ENCODING , "gzip"); 
	
		## Below two option will enable the HTTPS option.
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,  2);   

		// set this to true and run on the command line to see the debug output. it isn't really usefull
		if ($this->debug)
		{
			curl_setopt($ch, CURLOPT_VERBOSE, true); 
		}
		if (is_array($this->headers))
		{
			curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers); 
		}

		if (strtoupper($method) == "POST")
		{
			if (! strstr($actualURL,'?'))
			{
				log::error("Please ADD post fields on the url string just as you would a get. This url has no post fields! $actualURL");
				#return;
			}
			list($url,$data) = explode('?',$actualURL);
			curl_setopt($ch, CURLOPT_POST, 1); 
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data); 

			if (! $this->sendQueryStringOnPost )
			{
				$actualURL = $url;
			}
		}
		curl_setopt($ch, CURLOPT_URL, $actualURL);
		return $ch;
	}

	// when threads is 1, use a simpler fetch
	public function singleFetch($url,$method)
	{
			
		$ch = $this->curlInit($url,$method);


		$html = curl_exec($ch);      
	
		curl_close($ch);

		return $html;
	}

	// when threads is 1, use a simpler fetch
	public function singleQueuedFetch($callback=NULL,$method)
	{
		$type = get_class($this);
		if ($callback==null)
		{
			$callback  = "$type::loadCallBack";
		}

		for(;;) 
		{
			$url = $this->queue->pop($type);
			$data = false;
			
			if (empty($url))
			{
				log::debug("Got empty URL from queue, returning. single threaded");
				$done = true;
				break;
			}

			$ch = $this->curlInit($url,$method);
			$html = curl_exec($ch);      
			curl_close($ch);

			call_user_func($callback, $url, $html, $type);
			flush();
		}

	}

   // for historical reasons this function defaults to GET. to do an actual post
   // put the post parameters on the url, and pass the POST method
   public function queuedFetch( $callback=NULL, $method='GET')
	{

		if ($callback==NULL)
		{
			$type = get_class($this);
			$callback  = "$type::loadCallBack";
		}

      $threads = $this->threads;

		$mcurl = curl_multi_init();
		$threadsRunning = 0;
		$curlMap = array();
      $done = false;

      $type = get_class($this);
      $guiReset=false;


      if (!$this->useCurl)
      {
         while (($url = $this->queue->pop($type)) != "")
         {
            $html = file_get_contents($url);
            $bytes=strlen($html);           
            call_user_func($callback, $url, $html, $type);
         }


      // Fill up the slots
      }
      else
      {     
         for(;;) 
         {
            if ($threadsRunning == $threads)
            {
               if ($guiReset)
               {
                  log::debug("Waiting for threads to complete.");
               }
               else
               {
                  log::debug(".",$rawEcho=true);
               }
               $guiReset=false;
            }
            else
            {
               $guiReset = true;
            }
            log::debug_verbose("Current thread count: $threadsRunning/$threads");
         
            while ($threadsRunning < $threads) 
            {
               log::debug("Running threaded post threads: $threadsRunning / $threads");

               $url = $this->queue->pop($type);
               $data = false;
               
               if (empty($url))
               {
                  log::debug("Got empty URL from queue, returning. threads: $threadsRunning/$threads");
                  $done = true;
                  break;
               }

               # remove any trailing # signs, but don't take ? query params out
               $actualURL = preg_replace("/\#.+\???/","",$url);
               $ch = $this->curlInit($actualURL,$method);

               curl_multi_add_handle($mcurl, $ch);
               $threadsRunning++;
               $curlMap[$ch] = $url;
            }
            // Check if done
            if ($threadsRunning == 0 && $done)
            {
               break;
            }

            // Let mcurl do it's thing
            curl_multi_select($mcurl);
            while(($mcRes = curl_multi_exec($mcurl, $mcActive)) == CURLM_CALL_MULTI_PERFORM) usleep(10000);
            if($mcRes != CURLM_OK) break;
            while($done = curl_multi_info_read($mcurl)) 
            {
               $ch = $done['handle'];
               $html = curl_multi_getcontent($ch);
               $url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);

               if(curl_errno($ch) == 0) 
               {
                  if ($this->debug)
                  {
                     file_put_contents("lastfetch.html",$html);
                  }
                  call_user_func($callback, $url, $html, $type);
                  flush();
               } 
               else 
               {
                  // echo "Link <a href='$done_url'>$done_url</a> failed: ".curl_error($ch)."<br>\n";
                  flush();
               }
               curl_multi_remove_handle($mcurl, $ch);
               curl_close($ch);
               $threadsRunning--;
            }
         }
         log::debug("Closing curl_multi");
         curl_multi_close($mcurl);
      }
	}

	/**
	 * CURL callback function for reading and processing headers
	 */
	private function readHeader($ch, $header)
	{
      $url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
      $this->responseHeaders[$url][] = $header;
		return strlen($header);
	}

   // this makes sure $url is set to $referer
   public function setReferer($url, $referer)
   {
      $this->referer[$url] = $referer;
   }

   public function getReferer($url)
   {
      if (isset($this->referer[$url]))
      {
         return $this->referer[$url];
      }
      else
      {
         if (isset($this->referer["*"]))
         {
            return $this->referer["*"];
         }
      }
      return "";
   }

   public function setCookies($data)
   {
      $cookies = array();
      foreach ($data as $key => $value)
      {
         $cookies[] = "$key=$value";
      }
      $this->cookieData = join(';',$cookies);
   }

	// this should be integrated into relative2absolute.
	// it turns keeps us from leaving the site.
	static function normalizeHref($base,$href)
	{
		// don't leave the site
		if (preg_match("/^\//", $href))
		{
			$href = "http://$base$href";
		}
		
		// don't leave the site 
		if(! preg_match("/$base/i", $href))
		{
			$href = "";
		}

		return $href;
	}
	// turn a relative path in to an fully qualified url.
	//  requires current url, and relative uri from a link.
	//    relative can be any src="" valid URI. 
	//		   ex: http://www.fqd.com/, /absolute/path/here , ../relative/path/, ?justThe=querystring
	static function relative2absolute($absolute, $relative ) 
	{
        $p = @parse_url($relative);
        if(!$p) {
	        //$relative is a seriously malformed URL
	        return false;
        }
        if(isset($p["scheme"])) return $relative;
 
        $parts=(parse_url($absolute));
 
        if(substr($relative,0,1)=='/') {
            $cparts = (explode("/", $relative));
            array_shift($cparts);
        } else {
            if(isset($parts['path'])){
                 $aparts=explode('/',$parts['path']);
                 array_pop($aparts);
                 $aparts=array_filter($aparts);
            } else {
                 $aparts=array();
            }
           $rparts = (explode("/", $relative));
           $cparts = array_merge($aparts, $rparts);
           foreach($cparts as $i => $part) {
                if($part == '.') {
                    unset($cparts[$i]);
                } else if($part == '..') {
                    unset($cparts[$i]);
                    unset($cparts[$i-1]);
                }
            }
        }
        $path = implode("/", $cparts);
 
        $url = '';
        if($parts['scheme']) {
            $url = "$parts[scheme]://";
        }
        if(isset($parts['user'])) {
            $url .= $parts['user'];
            if(isset($parts['pass'])) {
                $url .= ":".$parts['pass'];
            }
            $url .= "@";
        }
        if(isset($parts['host'])) {
            $url .= $parts['host']."/";
        }
        $url .= $path;

		  // don't leave this site.
 
        return $url;
	}

	// a version of http_build_query that works properly.
	#	urlencode
	#		0	both (default)
	#		1  key
	#		2  value
	#		3  neither
	#
	function buildQuery($data,$urlencode=0)
	{
		foreach ($data as $k => $v)
		{
			if ($urlencode == 0 || $urlencode == 1)
			{
				$k = urlencode($k);
			}
			if ($urlencode == 0 || $urlencode == 2)
			{
				$v = urlencode($v);
			}
			$query[] = "$k=$v";
		}
		return join("&",$query);
	}

	// fast way to extract variables from a query string or url.
	static function urlVar($url,$var)
	{
		parse_str(parse_url( $url,PHP_URL_QUERY),$data);


		if (array_key_exists($var,$data))
			return $data[$var];
		else
			return false;
	}

	function saveZip($path)
	{
		$name = get_class($this);
		@unlink("$path\\$name.zip");
		exec("zip -9 -D -o $path\\$name.zip csv\\$name.csv");
	}

}
